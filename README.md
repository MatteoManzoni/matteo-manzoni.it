# My Personal Site
## *Just hire me already*

### Table of contents
- [Table of contents](#table-of-contents)
    - [Just A Screen](#just-a-screen)
    - [Why](#why)
    - [Sorry PHP](#sorry-php)
    - [How To Help](#how-to-help)
    - [How To Use](#how-to-use)



### Just A Screen
![Just A Screen](res/latest_screen.png)



### Why
*Bored and underemployed people do bored and underemployed stuff (plus a lazy boy CI), **but you can change it today!***



### Sorry PHP
I used PHP for this project only because i needed some simple mail/curl functionality without thinking much about it but I also think that PHP is quite a dead language and *I don't want to be part of his legacy*
> PHP is dead, long live PHP



### How To Help
You can help me by visiting my site, forking it or simply by telling me what you think.

[Aforementioned WebSite](https://matteo-manzoni.it "Just your average awesome website")



### How To Use
- Just fork, clone, download, ask-me-to-ship-this-repo-to-you-in-a-usb-drive
- Change remote
- Change environment CI variables as needed
- Push it
- Your new site should be UP

*If you really want to use it you can even modify the `static content` and `styles`*
