<?php
  session_start();
  
  $client_ip=get_client_ip();
  mail_me_ip($client_ip);

  include('static_content.html');

    echo'
     <script>
       window.onscroll = function () { myFunction() };

       var navbar = document.getElementById("navbar");
       var sticky = navbar.offsetTop;

       function myFunction() {
           if (window.pageYOffset >= sticky) {
               navbar.classList.add("sticky")
           } else {
               navbar.classList.remove("sticky");
           }
       }

       window.onload = function () {
           scrollSpy("#nav", {
               offset: 100,
               menuActiveTarget: "a",
               sectionClass: ".scrollspy",
               activeClass: "active",
           })';
           
     if(isset($_SESSION["sent"])&&$_SESSION["sent"]==true){
       unset($_SESSION["sent"]);

       echo'
           var x = document.getElementById("snackbar");
           x.className = "show";
           setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);';
     }
     if(isset($_SESSION["sent"])&&$_SESSION["sent"]==false)
     {
       unset($_SESSION["sent"]);
       echo'
           var x = document.getElementById("snackbarFalse");
           x.className = "showFalse";
           setTimeout(function(){ x.className = x.className.replace("showFalse", ""); }, 3000);';
     }
  echo'
      }
    </script>
  </html>
  ';


  function get_client_ip() {
      $ipaddress = '';
      if (isset($_SERVER['HTTP_CLIENT_IP']))
          $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
      else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
          $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
      else if(isset($_SERVER['HTTP_X_FORWARDED']))
          $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
      else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
          $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
      else if(isset($_SERVER['HTTP_FORWARDED']))
          $ipaddress = $_SERVER['HTTP_FORWARDED'];
      else if(isset($_SERVER['REMOTE_ADDR']))
          $ipaddress = $_SERVER['REMOTE_ADDR'];
      else
          $ipaddress = 'UNKNOWN';
      return $ipaddress;
  }

  function mail_me_ip($ip_client) {
    $headers = "From: {MY_NAME} <{MAIL_FROM}>\r\n";
    $headers .= "X-Priority: 2\r\n"; // 2 = urgent, 3 = normal, 4 = low
    $headers .= "X-Mailer: PHP/" . phpversion();

    $destinatario = "{MAIL_TO}";

    $subject = "{SITE_URL} connection";


    $message = "
      This message was sent after a connection to {SITE_URL}\n
      Connection from: ".$ip_client."\n
      WHOIS:\n
      ".getUrlContent($ip_client);

      $params = "-f {MAIL_FROM}";

      mail ($destinatario, $subject, $message, $headers, $params);
  }



  function getUrlContent($ip_client)
  {  
    $api_url = "cli.fyi/".$ip_client;

    if( !extension_loaded('curl') ){die('You need to load/activate the cURL extension (http://www.php.net/cURL).'); }

      $ch = curl_init();  
      curl_setopt($ch, CURLOPT_URL, $api_url); // set the url to fetch
      curl_setopt($ch, CURLOPT_HEADER, 0); // set headers (0 = no headers in result)
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // type of transfer (1 = to string)
      curl_setopt($ch, CURLOPT_USERAGENT,'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1');
      curl_setopt($ch, CURLOPT_TIMEOUT, 10); // time to wait in seconds
      $content = curl_exec($ch); // make the call  
      curl_close($ch);  
      return $content;
  } 

?>
							
