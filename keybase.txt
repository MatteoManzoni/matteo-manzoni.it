==================================================================
https://keybase.io/hp_inkjet
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://matteo-manzoni.it
  * I am hp_inkjet (https://keybase.io/hp_inkjet) on keybase.
  * I have a public key ASB4911D5vWYcHikst3RrCOWfrqwAlvi-vvPEV6kKv7GmAo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "01209f88c7e291cb9eb5b2feb2fa44020f8497f95e5daef4e3e39bfd16fd53ed90320a",
      "host": "keybase.io",
      "kid": "012078f75d43e6f5987078a4b2ddd1ac23967ebab0025be2fafbcf115ea42afec6980a",
      "uid": "1ba16f9e0041fe32dbbfde21f25e7e19",
      "username": "hp_inkjet"
    },
    "merkle_root": {
      "ctime": 1543606750,
      "hash": "ff3beee58a3a4af0c9b7fee96934c529236bcae62cd1984b6cf82f463eb2d6868c38a487745dc78bee3bdab464d934710ed199ddac68bae1cb9c9ca54375cd47",
      "hash_meta": "6d724ff6ca619e56ecaa0b3767e9856b4793802322ef4b1f76d7917ec98223b7",
      "seqno": 4033843
    },
    "service": {
      "entropy": "sZqpgZYUsxDpk/ozBQMb9Ppd",
      "hostname": "matteo-manzoni.it",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "2.11.0"
  },
  "ctime": 1543606767,
  "expire_in": 504576000,
  "prev": "139539702b3292c9ab5ffbb7e71454e9ef3d385c8b11e215453b4d0416406541",
  "seqno": 7,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEgePddQ+b1mHB4pLLd0awjln66sAJb4vr7zxFepCr+xpgKp3BheWxvYWTESpcCB8QgE5U5cCsyksmrX/u35xRU6e89OFyLEeIVRTtNBBZAZUHEIKsn63Xq1nXG7sPyww3O8evBdjyzfIvM1iUlb3vMfnmHAgHCo3NpZ8RATFyfX3APJyUEqNqLX488GeYJ1AmnSrUMwiRuf0aa3VJEu9D96ds0T4vmyoczYPCA6EhcCQnoY5NWtUi/DQKeDqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEIL9zkiQyTWS3/uLMOA4TOfgyDS4rZxyL8GPZ8Wsv4YSyo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/hp_inkjet

==================================================================
