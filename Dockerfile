FROM php:7.2-apache

MAINTAINER Matteo Manzoni <manzoni.matteo@mailfence.com>

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

COPY * /var/www/html/
